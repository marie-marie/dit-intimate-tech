* [**How to organize a CryptoParty** ](https://www.cryptoparty.in/organize/howto) This How To is written to give you some ideas on what might work when organizing a CryptoParty and what not so much. As time has shown there are some general tactics which make everything go much smoother.
* [**Surveillance self-defense**](https://ssd.eff.org/) A project of the electronic frontier foundation with tips, tools and how-tos for safer online communications as an expert guide to protecting you and your friends from online spying.
* [**Me and my shadow**](https://myshadow.org/) A project of the Tactical Technology Collective that helps you control your data traces, see how you're being tracked, and learn more about the data industry.
* [**Sexy Guide to Safer Nudes**](https://www.codingrights.org/safernudes/) A project by Coding Rights, an organisation bringing an intersectional feminist approach to defend human rights in the development, regulation and use of technologies.

