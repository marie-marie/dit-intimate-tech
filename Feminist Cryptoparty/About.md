### What's a CryptoParty

CryptoParty is a decentralized, global initiative to introduce basic tools for protecting privacy, anonymity and overall security on the Internet to the general public.

### Why

Privacy is the room in which ideas develop, where you can reflect on those ideas whenever you choose. This room is not only physical, but digital as well. The goal of this workshop is to pass on knowledge about protecting yourself and your communities in the digital space with a feminist perspective. This can include encrypted communication, preventing being tracked while browsing the web, safer sexting, and general security advice regarding computers and smartphones.

### What

In this workshop a simple framework for making informed choices on which tools to use in the digital space will be introduced. We will also go into further detail and discuss different ways of protecting yourself and your communities in the digital space from a feminist perspective.
How do the digital tools and services that we use each day differ from each other and why does it matter? The workshop is an invitation to collaborate on creating an environment where people feel welcome and safe to learn and teach no matter what their background or level of expertise is. All questions are relevant and all explanation shall be tailored to the person with the least prior knowledge. Bring your laptop or smartphone
 the Weizenbaum Institute's Research Groups 2


Feminist Cryptoparty
Wednesday, 16.10.2019, 18.00 – 21.00
Berlin Open Lab, Einsteinufer 43
Weizenbaum Institute's research group 2 [„Production Possibilities of the Maker Culture“](https://www.weizenbaum-institut.de/en/research/work-innovation-and-value-creation/production-possibilities-of-the-maker-culture/) in collaboration with [Berlin cryptoangels](https://www.cryptoparty.in/)

Feminist Cryptoparty „Digital Tools“
Wednesday, 27.11.2019, 18.00 – 21.00
Berlin Open Lab, Einsteinufer 43
Weizenbaum Institute's research group 2 [„Production Possibilities of the Maker Culture“](https://www.weizenbaum-institut.de/en/research/work-innovation-and-value-creation/production-possibilities-of-the-maker-culture/) and research group 8 ["Inequality and Digital Sovereignty"](https://www.weizenbaum-institut.de/en/research/rg8/)in collaboration with [Berlin cryptoangels](https://www.cryptoparty.in/)
 
