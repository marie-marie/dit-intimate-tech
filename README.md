# What
Everyday intimate technologies.
To increase body awareness we want to explore the possibilities of qualitative approach to observing hormonal changes (e.g.g. in urine) that may reflect relative changes over a longer period of time. 
Similarly to regularly checking breasts for lumps the observation and DIY analysis of (e.g.) urine can give first hints that something might be unusual. 
Prototypes and approaches that alter the use, deployment or perception of technology.

# Why
This can be an indication that leads to checking in with medical experts. Observing hormonal changes are relevant because their linked to different aspects of overall health.
The Prototypes focus on explorations rather than on delivering results. We are as interested in technology as in the complex relationships between society and our body(fluids). 

# About

This project and the associated workshops were carried out in the framework of the research group ["Production Possibilities of the Maker Culture"](https://weizenbaum-institut.de/en/research/rg2/) at the [Berlin University of the Arts](https://www.udk-berlin.de/en/home/) / [Weizenbaum Institute](https://www.weizenbaum-institut.de/index.php?id=1&L=5).