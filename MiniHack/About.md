# About MiniHack

## Context

While long being niche technologies for decision-making powers in the tech world, period tracking apps have evolved to being part of the current default set of health apps and so-called Femtech is on the rise. Beside that, multiple products are flooding the market that promise monitor PMS, ovulation and moods, to contracept and enhance people’s experience of their menstrual cycle. The commercialization of technologies does not spare the intimacy of health data from being exploited for advertisement.
With the MiniHack we want to explore potentials of Open Soft- and Hardware to envision and speculate possibilities to experience ovulation in the realms of self-souvereignity and self-quantification, commericialization of body ata in neoliberal times and reinforcement of Gender stereotypes.

Links to articles and ressources on this are to be found under [FurtherReadings.md](https://gitlab.com/marie-marie/dit-intimate-tech/-/blob/master/MiniHack/FurtherReadings.md).

## What

What if we knew exactly when we ovulate?

This MiniHack is a workshop for researchers, tinkerers and hackers, who are invited to come together and create ideas with pen & paper, different sensors and materials. Which hardware, which sensors could help us to experience ovulation? The second half of the MiniHack is about speculative thought experiments on the practicability of such hardware, how should it feel, how would it integrate into everyday life. But also: Which effects and consequences would the self-knowledge of ovulation have both on an individual and collective level?

MiniHack: Menstrual Cycle & Ovulation
July 10th, 2019, 6-9pm
[Berlin Open Lab](https://berlin-open-lab.org/), Einsteinufer 43, 10587 Berlin